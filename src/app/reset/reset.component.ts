import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ActivatedRoute} from "@angular/router";
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  token: String = '';
  password: String = '';
  confirmPassword: String = '';

  constructor(private http: HttpClient, private route: ActivatedRoute, public snackBar: MatSnackBar) {
    this.route.params.subscribe( params => {
      this.token = params.token
    });
  }

  ngOnInit() {
  }

  resetPassword() {
    this.http.post('https://iconikstudioapp.com/auth/password-reset', {
      token: this.token,
      password: this.password,
      confirmPassword: this.confirmPassword
    })
    .toPromise()
    .then((response) => {
      this.snackBar.open('Your password has been reset!', '', {
        duration: 3000,
      });
    })
    .catch(() => {
      this.snackBar.open('Failed to reset password', '', {
        duration: 3000,
      });
    });
  }

}
