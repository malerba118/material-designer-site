import { Component, ViewChild, HostListener, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material';
// import libgif from '../assets/js/libgif'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'app';
  @ViewChild('myGif') myGif;
  @ViewChild('scroller') scroller;
  @ViewChild('scrollerContent') scrollerContent;
  sup1: any = {finishedLoading:false};
  sheet1: any;
  sheet2: any;
  sheet3: any;
  sheet4: any;
  sheet5: any;
  progress: number = 0;
  carouselPosition: number = 0;
  demo: any;
  showCarouselButtons: boolean = false;

  phoneContainer: any;
  autoScroller: any;

  constructor(public snackBar: MatSnackBar) {
    this.sheet1 = {
      shouldBeHidden: false
    }
    this.sheet2 = {
      shouldBeHidden: false
    }
    this.sheet3 = {
      shouldBeHidden: false
    }
    this.sheet4 = {
      shouldBeHidden: false
    }
    this.sheet5 = {
      shouldBeHidden: false
    }
    this.phoneContainer = {
      shouldCoverPage: true
    }
    this.demo = {
      shouldBeHidden: false
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // Ha! take that, linter
    var sup1 = new window['SuperGif']({ gif: this.myGif.nativeElement, max_width: 320 }, (progress, total) => {
      this.progress = Math.round(progress/total*100);
    });
    sup1.finishedLoading = false;
    this.sup1 = sup1
	  sup1.load(function(){
      sup1.finishedLoading = true;
		}, console.log);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    //On window resize, adjust splitpane view as necessary
    if (this.scroller.nativeElement.scrollTop/30 > 0 && window.innerWidth > 700) {
      this.phoneContainer.shouldCoverPage = false
    }
    else {
      this.phoneContainer.shouldCoverPage = true
    }
    if (this.demo.shouldBeHidden && window.innerWidth < 650) {
      this.showCarouselButtons = true
    }
    else {
      this.showCarouselButtons = false
    }
  }

  carouselPrev() {
    this.carouselPosition = Math.max(this.carouselPosition - 1, -1)
  }

  carouselNext() {
    this.carouselPosition = Math.min(this.carouselPosition + 1, 1)
  }

  onScroll($event) {
      if (this.sup1.finishedLoading) {
        this.sup1.move_to(Math.floor(this.scroller.nativeElement.scrollTop/15));
      }
      // On small screens, don't show split pane
      if (this.scroller.nativeElement.scrollTop/30 > 0 && window.innerWidth > 700) {
        this.phoneContainer.shouldCoverPage = false
      }
      else {
        this.phoneContainer.shouldCoverPage = true
      }
      if (this.scroller.nativeElement.scrollTop/30 > 15) {
        this.sheet1.shouldBeHidden = true
      }
      else {
        this.sheet1.shouldBeHidden = false
      }
      if (this.scroller.nativeElement.scrollTop/30 > 36) {
        this.sheet2.shouldBeHidden = true
      }
      else {
        this.sheet2.shouldBeHidden = false
      }
      if (this.scroller.nativeElement.scrollTop/30 > 58) {
        this.sheet3.shouldBeHidden = true
      }
      else {
        this.sheet3.shouldBeHidden = false
      }
      if (this.scroller.nativeElement.scrollTop/30 > 80) {
        this.sheet4.shouldBeHidden = true
      }
      else {
        this.sheet4.shouldBeHidden = false
      }
      if (this.scroller.nativeElement.scrollTop + innerHeight >= 4000) {
        this.demo.shouldBeHidden = true
      }
      else {
        this.demo.shouldBeHidden = false
      }
      if (this.demo.shouldBeHidden && window.innerWidth < 650) {
        this.showCarouselButtons = true
      }
      else {
        this.showCarouselButtons = false
      }
  }

  startScroll() {
    if (!this.autoScroller) {
      this.autoScroller = setInterval(() => {
        var top = this.scroller.nativeElement.scrollTop;
        var clientHeight = this.scroller.nativeElement.clientHeight;
        if (top + innerHeight >= this.scrollerContent.nativeElement.scrollHeight) {
          this.stopScroll()
        }
        else {
          this.scroller.nativeElement.scrollTop += 2
        }
      }, 18)
    }
  }

  stopScroll() {
    clearInterval(this.autoScroller)
    delete this.autoScroller
  }

  handlePlatformClick() {
    this.snackBar.open('Coming soon!', '', {
      duration: 3000,
    });
  }
}
