import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component'
import { TermsComponent } from '../terms/terms.component'
import { PrivacyComponent } from '../privacy/privacy.component'
import { ResetComponent } from '../reset/reset.component'


const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
    },
    {
        path: 'terms',
        component: TermsComponent,
    },
    {
        path: 'privacy',
        component: PrivacyComponent,
    },
    {
        path: 'password-reset/:token',
        component: ResetComponent,
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
